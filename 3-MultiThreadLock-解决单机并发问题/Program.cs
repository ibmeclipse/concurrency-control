﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace _3_MultiThreadLock_解决单机并发问题
{
    class Program
    {
        //锁-- 要保证系统里唯一，在Web项目中，实现方式：单例模式
        private static readonly object resource = new object();

        static void Main(string[] args)
        {
            Console.WriteLine("多线程处理方法一：加锁处理，\n 锁资源要保证系统唯一，在web上的实现方式即用单例模式---：启动两个线程，每个线程循环100次，理论(预期)上是结果是200，实际结果200，\n 满足要求，但性能差了点，优化方法：队列");

            //启两个线程
            new Thread(Run).Start();
            new Thread(Run).Start();

            //for (int i = 0; i < 100; i++)
            //{
            //    new Thread(Run).Start();
            //}


        }

        public static void Run()
        {
            for (int i = 1; i <= 100; i++)
            {
                lock (resource)
                {
                    var total = DbHelper.ExecuteScalar("Select Total from ConCurrency where Id = 1", null).ToString();
                    var value = int.Parse(total) + 1;
                    DbHelper.ExecuteNonQuery(string.Format("Update ConCurrency Set Total = {0} where Id = 1", value.ToString()), null);
                }
                //Thread.Sleep(1);
            }
        }
    }
}
