﻿using Common;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _6_RabbitMQ_Receive
{
    class Program
    {
        private static ConnectionFactory factory = new ConnectionFactory { HostName = "118.89.243.119", UserName = "guest", Password = "guest", VirtualHost = "/" };
        static void Main(string[] args)
        {
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    var consumer = new EventingBasicConsumer(channel);
                    consumer.Received += (model, ea) =>
                    {
                        var body = ea.Body;
                        var message = Encoding.UTF8.GetString(body);
                        Console.WriteLine(" [x] Received {0}", message);

                        var total = DbHelper.ExecuteScalar("Select Total from ConCurrency where Id = 1", null).ToString();
                        var value = int.Parse(total) + 1;

                        DbHelper.ExecuteNonQuery(string.Format("Update ConCurrency Set Total = {0} where Id = 1", value.ToString()), null);
                    };

                    channel.QueueDeclare(queue: "queueName", durable: false, exclusive: false, autoDelete: false, arguments: null);
                    channel.BasicConsume(queue: "queueName", noAck: true, consumer: consumer);

                    Console.WriteLine(" Press [enter] to exit.");
                    Console.ReadLine();
                }
            }
        }
    }
}
