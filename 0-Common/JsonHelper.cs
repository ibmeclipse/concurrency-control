﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class JsonHelper
    {

        /// <summary>
        /// 序列化成string
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string SerializeObject(object obj)
        {
            return JsonConvert.SerializeObject(obj);
            //JsonConvert.DeserializeObject<Telunsu>
        }

        /// <summary>
        /// 序列化成JObject对象
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static JObject DeserializeObjectReturnJObject(object obj)
        {
            var str= JsonConvert.SerializeObject(obj);
            return (JObject)JsonConvert.DeserializeObject(str);
        }
        /// <summary>
        /// string 反序列化成对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T DeserializeObject<T>(string value)
        {
            return JsonConvert.DeserializeObject<T>(value);
        }

        public static object DeserializeObject(string value)
        {
            return JsonConvert.DeserializeObject(value);
        }

    }
}
