﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
namespace  Common
{
    /// <summary>
    /// 使用LOG4NET记录日志的功能，在WEB.CONFIG里要配置相应的节点
    /// </summary>
    public class LogHelper
    {
        //log4net日志专用
        private static readonly log4net.ILog loginfo = log4net.LogManager.GetLogger("LogInfo");
        private static readonly log4net.ILog logdebug = log4net.LogManager.GetLogger("LogDebug");

        public static void SetConfig()
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        public static void SetConfig(FileInfo configFile)
        {
            log4net.Config.XmlConfigurator.Configure(configFile);
        }

        /// <summary>
        /// 调试的文件记录日志
        /// </summary>
        /// <param name="info"></param>
        public static void Debug(object info)
        {
            if (logdebug.IsDebugEnabled)
            {
                logdebug.Debug(JsonHelper.SerializeObject(info));
            }
        }

        public static void Debug(string debug, object info)
        {
            if (logdebug.IsDebugEnabled)
            {
                logdebug.Debug(debug + ":" + JsonHelper.SerializeObject(info));
            }
        }

        public static void Debug(string debug, string info)
        {
            if (logdebug.IsDebugEnabled)
            {
                logdebug.Debug(debug + ":" + info);
            }
        }
        /// <summary>
        /// 普通的文件记录日志（INFO）
        /// </summary>
        /// <param name="info"></param>
        public static void Log(object info)
        {
            if (loginfo.IsInfoEnabled)
            {
                loginfo.Info(JsonHelper.SerializeObject(info));
            }
        }
        /// <summary>
        /// 普通的文件记录日志（ERROR）
        /// </summary>
        /// <param name="info"></param>
        /// <param name="se"></param>
        public static void Log(object error, Exception se)
        {
            if (loginfo.IsErrorEnabled)
            {
                loginfo.Error(error, se);
            }
        }
    }
}
