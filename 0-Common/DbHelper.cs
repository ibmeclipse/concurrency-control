﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;//using: 引入命名空间

namespace Common
{
    public static class DbHelper
    {
        private static readonly string connectionString = "server=192.168.100.10;uid=sa;password=Dmea1526;database=ConCurrency";
        ////场景：
        ////1、有数据表：ConCurrency，

        // CREATE TABLE [dbo].[ConCurrency](
        //     [ID] [int] NOT NULL,
        //    [Total] [int] NULL
        // )

        ////2、初始值：ID=1,Total = 0
        ////3、现要求每一次客户端请求Total + 1


        /// <summary>
        /// 执行带参数的非查询SQL
        /// </summary>
        /// <param name="strSQL">SQL语句</param>
        /// <param name="values">参数</param>
        /// <returns>受影响行数</returns>
        public static int ExecuteNonQuery(string sql, params SqlParameter[] values)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    try
                    {
                        conn.Open();//打开数据源连接
                        if (values != null)
                        {
                            cmd.Parameters.AddRange(values);
                        }
                        int rows = cmd.ExecuteNonQuery();//执行SQL语句,返回受影响的行数。如rows>0，说明执行成功
                        return rows;
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        conn.Close();//出异常,关闭数据源连接
                        throw new Exception(string.Format("执行{0}失败:{1}", sql, ex.Message));
                    }
                }
            }
        }

        public static object ExecuteScalar(string sql, params SqlParameter[] values)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    try
                    {
                        conn.Open();//打开数据源连接
                        if (values != null)
                        {
                            cmd.Parameters.AddRange(values);
                        }
                        var result = cmd.ExecuteScalar();
                        return result;
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        conn.Close();//出异常,关闭数据源连接
                        throw new Exception(string.Format("执行{0}失败:{1}", sql, ex.Message));
                    }
                }
            }

        }
    }
}