﻿using NServiceKit.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class RedisBuilder
    {
        private static NServiceKit.Redis.IRedisClientsManager clientsManager;

        private static readonly object lockObj = new object();
        //private static readonly object lockAnalyticsObj = new object();

        public static NServiceKit.Redis.IRedisClientsManager GetClientsManager()
        {
                lock (lockObj)
                {
                    if (clientsManager == null)
                    {
                        clientsManager = new PooledRedisClientManager(
                            new[] { Config.Master_Redis },
                            new string[] { Config.Slave_Redis},
                            new RedisClientManagerConfig()
                            {
                                MaxWritePoolSize = Config.Redis_MaxWritePoolSize,
                                MaxReadPoolSize = Config.Redis_MaxReadPoolSize,
                            });
                    }
                }
            return clientsManager;
        }
    }

}
