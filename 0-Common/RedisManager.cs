﻿using NServiceKit.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
   public class RedisManager
    {
       private static NServiceKit.Redis.IRedisClientsManager clientsManager = RedisBuilder.GetClientsManager();

       public static IRedisClient GetClient(){
         return  clientsManager.GetClient();
       }
    }
}
