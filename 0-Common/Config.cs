﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Common
{
  public  class Config
    {
      //主Redis地址
      public static string Master_Redis
      {
          get
          {
              return ConfigurationManager.AppSettings["Master_Redis"];
          }
      }

      //从Redis地址
      public static string Slave_Redis
      {
          get
          {
              return ConfigurationManager.AppSettings["Slave_Redis"];
          }
      }

      /// <summary>
      /// Redis最大可写线程池大小
      /// </summary>
      public static int Redis_MaxWritePoolSize
      {
          get
          {
              return 200;
          }

      }

      /// <summary>
      /// Redis 最大可度线程池大小
      /// </summary>
      public static int Redis_MaxReadPoolSize
      {
          get
          {
              return 200;
          }

      }
    }
}
