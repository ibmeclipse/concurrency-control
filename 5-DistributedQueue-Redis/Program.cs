﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace _5_DistributedQueue_Redis
{
    class Program
    {
        static void Main(string[] args)
        {

            new Thread(ProduceToRedis).Start();

            new Thread(ProduceToRedis).Start();

            Thread.Sleep(1000 * 10);

            ConsumeFromRedis();

        }


        /// <summary>生产者</summary>
        public static void ProduceToRedis()
        {
            using (var client = RedisManager.GetClient())
            {
                for (int i = 1; i <= 100; i++)
                {
                    client.EnqueueItemOnList("EnqueueName", i.ToString());
                }
            }
        }

        /// <summary>消费者</summary>
        public static void ConsumeFromRedis()
        {
            using (var client = RedisManager.GetClient())
            {
                while (client.GetListCount("EnqueueName") > 0)
                {
                    if (client.SetEntryIfNotExists("lock", "lock"))
                    {
                        var item = client.DequeueItemFromList("EnqueueName");
                        var total = DbHelper.ExecuteScalar("Select Total from ConCurrency where Id = 1", null).ToString();
                        var value = int.Parse(total) + 1;

                        DbHelper.ExecuteNonQuery(string.Format("Update ConCurrency Set Total = {0} where Id = 1", value.ToString()), null);

                        client.Remove("lock");
                    }

                    Thread.Sleep(5);
                }
            }
        }
    }
}
