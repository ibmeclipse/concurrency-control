﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace MultiThread
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("多线程处理---有问题：启动两个线程，每个线程循环100次，理论(预期)上是结果应该是200，由于并发原因，结果<200");
            
            //启两个线程
            new Thread(Run).Start();

            new Thread(Run).Start();



        }

        public static void Run()
        {
            for (int i = 1; i <= 100; i++)
            {
                var total = DbHelper.ExecuteScalar("Select Total from ConCurrency where Id = 1", null).ToString();
                var value = int.Parse(total) + 1;
                DbHelper.ExecuteNonQuery(string.Format("Update ConCurrency Set Total = {0} where Id = 1", value.ToString()), null);
                Thread.Sleep(1);
            }
        }
    }
}
