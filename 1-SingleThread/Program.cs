﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Common;

namespace SingleThread
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("单线程处理---没问题");
            new Thread(Run).Start();

        }

        public static void Run()
        {
            for (int i = 1; i <= 100; i++)
            {
                var total = DbHelper.ExecuteScalar("Select Total from ConCurrency where Id = 1", null).ToString();
                var value = int.Parse(total) + 1;

                DbHelper.ExecuteNonQuery(string.Format("Update ConCurrency Set Total = {0} where Id = 1", value.ToString()), null);
                Thread.Sleep(1);
            }

        }
    }
}
