﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6_RabbitMQ_Send
{
    class Program
    {
        private static ConnectionFactory factory = new ConnectionFactory { HostName = "118.89.243.119", UserName = "guest", Password = "guest", VirtualHost = "/" };
        static void Main(string[] args)
        {
            for (int i = 1; i <= 500; i++)
            {
                Task.Run(async () =>
                {
                    await Produce();
                });
                Console.WriteLine(i);
            }
            Console.ReadKey();
        }
        public static Task Produce()
        {
            return Task.Factory.StartNew(() =>
            {
                using (var connection = factory.CreateConnection())
                {
                    using (var channel = connection.CreateModel())
                    {
                        var body = Encoding.UTF8.GetBytes(Guid.NewGuid().ToString());
                        channel.QueueDeclare(queue: "queueName", durable: false, exclusive: false, autoDelete: false, arguments: null);
                        channel.BasicPublish(exchange: "", routingKey: "queueName", basicProperties: null, body: body);
                    }
                }
            });
        }
    }
}
