﻿using Common;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace _4_ConcurrentQueue_解决单机并发加锁性能问题
{
    class Program
    {
        //线程安全队列
        //保证系统中线程队列唯一：Web中需要使用单例模式实现
        //在Java多线程应用中，队列的使用率很高，多数生产消费模型的首选数据结构就是队列。Java提供的线程安全的Queue可以分为阻塞队列和非阻塞队列，其中阻塞队列的典型例子是BlockingQueue，非阻塞队列的典型例子是ConcurrentLinkedQueue，在实际应用中要根据实际需要选用阻塞队列或者非阻塞队列

        static ConcurrentQueue<int> queue = new ConcurrentQueue<int>();
        static void Main(string[] args)
        {
            //生产
            new Thread(Produce).Start();
            //生产
            new Thread(Produce).Start();
            //消费
            //Web中需要将消费者另起一个服务，这是这个流程将变成了异步
            Consume();
        }

        /// <summary>生产者</summary>
        public static void Produce()
        {
            for (int i = 1; i <= 100; i++)
            {
                queue.Enqueue(i);
            }
        }

        /// <summary>消费者</summary>
        public static void Consume()
        {
            int times;
            while (queue.TryDequeue(out times))
            {
                var total = DbHelper.ExecuteScalar("Select Total from ConCurrency where Id = 1", null).ToString();
                var value = int.Parse(total) + 1;

                DbHelper.ExecuteNonQuery(string.Format("Update ConCurrency Set Total = {0} where Id = 1", value.ToString()), null);
                Thread.Sleep(1);
            }
        }
    }
}
