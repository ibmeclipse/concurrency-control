﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fractalist.Weixin.SDK.Config
{
   public class WeixinConfig
    {
        #region 微信配置
        /// <summary>
        /// 公众号后台配置的Token加密使用
        /// </summary>
        public static string Token
        {
            get
            {
                return ConfigurationManager.AppSettings["Token"];
            }

        }

        public static string EncodingAESKey
        {
            get
            {
                return ConfigurationManager.AppSettings["EncodingAESKey"];
            }
        }
        /// <summary>
        /// 微信公众号AppID
        /// </summary>
        public static string AppID
        {
            get
            {
                return ConfigurationManager.AppSettings["AppID"];
            }

        }

        public static string AppSecret
        {
            get
            {
                return ConfigurationManager.AppSettings["AppSecret"];
            }

        }
        public static string Host
        {
            get
            {
                return ConfigurationManager.AppSettings["Host"];
            }

        }
        #endregion
    }
}
