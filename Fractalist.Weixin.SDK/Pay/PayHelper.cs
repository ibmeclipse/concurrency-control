﻿using Fractalist.Library.http;
using Fractalist.Library.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fractalist.Weixin.SDK.Pay
{
    public class PayHelper
    {

        #region V3 统一支付接口

        /// <summary>
        /// 
        /// </summary>
        /// <param name="postData"></param>
        /// <param name="isApp"></param>
        /// <returns></returns>
        public static UnifiedPrePayMessage UnifiedPrePay(string postData)
        {
            string url = WeiXinConst.WeiXin_Pay_UnifiedPrePayUrl;
            var message = HttpClientHelper.PostXmlResponse<UnifiedPrePayMessage>(url, postData);
            return message;
        }

        #endregion

        #region V2&V3 订单查询

        #region V3 订单查询

        /// <summary>
        /// V3 统一订单查询接口
        /// </summary>
        /// <param name="postXml"></param>
        /// <returns></returns>
        public static UnifiedOrderQueryMessage UnifiedOrderQuery(string postXml)
        {
            string url = WeiXinConst.WeiXin_Pay_UnifiedOrderQueryUrl;
            UnifiedOrderQueryMessage message = HttpClientHelper.PostXmlResponse<UnifiedOrderQueryMessage>(url, postXml);
            return message;
        }

        #endregion


        #endregion


        #region V3 申请退款

        /// <summary>
        /// 申请退款（V3接口）
        /// </summary>
        /// <param name="postData">提价XML数据</param>
        /// <param name="timeOut">超时时间</param>
        /// <returns></returns>
        public static RefundMessage Refund(string postData, int timeOut = 6)
        {
            string url = WeiXinConst.WeiXin_Pay_UnifiedOrderRefundUrl;
            RefundMessage message = HttpClientHelper.PostXmlResponse<RefundMessage>(url, postData);//调用HTTP通信接口提交数据到API
            return message;
        }
        #endregion



        #region 微信中推送消息

        /// <summary>
        /// 微信中推送消息
        /// </summary>
        /// <param name="FromUserName"></param>
        /// <param name="ToUserName"></param>
        /// <param name="msg"></param>
        public static string ResponseMessage(string FromUserName, string ToUserName, string msg)
        {
            string aurl = string.Format(@"{0}", msg);
            string message = @"" + msg;
            string postXmlStr2 = @"<xml>
                                                    <ToUserName><![CDATA[" + FromUserName + @"]]></ToUserName>
                                                    <FromUserName><![CDATA[" + ToUserName + @"]]></FromUserName>
                                                    <CreateTime>" + ConvertDateTimeInt(DateTime.Now) + @"</CreateTime>
                                                    <MsgType><![CDATA[text]]></MsgType>
                                                    <Content><![CDATA[" + message + @"]]></Content></xml>";
            return postXmlStr2;
        }

        public static int ConvertDateTimeInt(System.DateTime time)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
            return (int)(time - startTime).TotalSeconds;
        }
        #endregion
    }
}
